package test_package;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

/**
 * Created by user on 9/11/17.
 */
public class TestNGLogout_class extends TestNGMain_class {

    @Test
    public void Log_out() throws Exception {
        Actions action = new Actions(driver);
        WebElement we = driver.findElement(By.cssSelector("li.hoverSelector"));
        action.moveToElement(we).moveToElement(driver.findElement(By.cssSelector("a.navbar-link"))).click().build().perform();
    }
}
