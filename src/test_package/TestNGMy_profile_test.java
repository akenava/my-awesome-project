package test_package;


import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.Test;

import java.io.Console;

public class TestNGMy_profile_test extends TestNGMain_class {


    @Test
    public void Patient_my_profile_account_details_page() throws Exception
    {

//        System.out.print(driver.findElement(By.xpath("//iframe[0]")));

        try
        {
            //driver.get(baseUrl + "es/dashboard");
            Thread.sleep(2000);
            Actions action = new Actions(driver);
            Thread.sleep(1000);
            WebElement we = driver.findElement(By.xpath("//li[3]"));
            action.moveToElement(we).moveToElement(driver.findElement(By.xpath("//li[3]/ul/li/a"))).click().build().perform();
            //System.out.print(driver.getPageSource());
//        driver.switchTo().defaultContent(); // you are now outside both frames

            Thread.sleep(2000);
            WebElement elem = (WebElement) driver.findElement(By.xpath("//iframe"));


            driver.findElement(By.id("id_email"));
            driver.findElement(By.id("id_first_name"));
            driver.findElement(By.id("id_last_name"));
            driver.findElement(By.id("id_second_last_name"));
            driver.findElement(By.id("id_birthday"));
            driver.findElement(By.id("id_gender"));
            driver.findElement(By.id("id_photo"));
            driver.findElement(By.id("id_phone_number"));
            driver.findElement(By.id("id_timezone"));
            driver.findElement(By.id("id_tax_number"));
        }
        catch(Exception e)
        {
            System.out.print(e);
            System.exit(1);
        }
    }

    @Test
    public void Patient_my_profile_payment_methods_page() throws Exception
    {
        Thread.sleep(2000);
        //WebElement elem2 = (WebElement) driver.findElement(By.xpath("//iframe"));

        //driver.switchTo().frame(driver.findElement(By.xpath("//iframe")));
        //driver.findElement(By.xpath("/*[contains(local-name(), 'iframe')]"));
        try
        {
            //driver.switchTo().defaultContent();
            driver.navigate().refresh();
           // WebElement elem = (WebElement) driver.findElement(By.xpath("//iframe"));
            driver.findElement(By.xpath("//a[@href='/es/payment-methods/']/..")).click();
            WebElement elem = (WebElement) driver.findElement(By.xpath("//a[@href='/es/payment-methods/create/']"));
            elem.click();
        }
        catch(Exception e)
        {
            System.out.print(e);
            System.exit(1);
        }
    }

    @Test
    public void Patient_profile_payment_history()
    {
        try
        {
            driver.findElement(By.xpath("//a[@href='/es/my_account/billing/']/../..")).click();
            driver.findElement(By.xpath("//div/div[2]/div[2]/div"));
        }
        catch (Exception e)
        {
            System.out.print(e);
            System.exit(1);
        }
    }

    @Test
    public void Patient_profile_password_change()
    {
        try
        {
            driver.findElement(By.xpath("//a[@href='/es/my_account/password/']/..")).click();
            driver.findElement(By.id("id_oldpassword"));
            driver.findElement(By.id("id_password1"));
            driver.findElement(By.id("id_password2"));
            driver.findElement(By.xpath("//button[@type='submit']"));
        }
        catch (Exception e)
        {
            System.out.print(e);
            System.exit(1);
        }
    }

}
