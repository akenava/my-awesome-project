package test_package;

import java.io.*;
import java.util.*;
import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import java.math.*;
import java.util.Random.*;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.*;

import static jdk.nashorn.internal.objects.Global.print;
import static net.sourceforge.htmlunit.corejs.javascript.TopLevel.Builtins.String;
import static org.testng.Assert.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class TestNGMain_class {
    static WebDriver driver;
    static String baseUrl;
    private boolean acceptNextAlert = true;
    private StringBuffer verificationErrors = new StringBuffer();
    static Random rnd = new Random();
    static String login;
    static  String pass;
    static String [] myArray = new String[2];

    @BeforeSuite
    static void setUp() throws Exception {
        driver  = new FirefoxDriver();
        baseUrl = "https://preprod.cmed-online.com/";
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        readFromFile();
    }

   /* @Test
    public void Fill_login_form() throws Exception {
        driver.get(baseUrl + "/es/dashboard/");
        driver.findElement(By.linkText("Iniciar sesión")).click();
        driver.findElement(By.id("id_password")).clear();
        driver.findElement(By.id("id_password")).sendKeys("zaq123");
        driver.findElement(By.id("id_login")).clear();
        driver.findElement(By.id("id_login")).sendKeys("parkhval@gm ail.com");
        driver.findElement(By.cssSelector("button.btn-u.btn-block")).click();
    }*/

    @AfterSuite
    public void tearDown() throws Exception {
       // driver.quit();
        String verificationErrorString = verificationErrors.toString();
        if (!"".equals(verificationErrorString)) {
            fail(verificationErrorString);
        }
    }

    public static String generateString() {
        String uuid = UUID.randomUUID().toString();
        return  uuid;
    }

    public static void writeToFile (String[] args) throws IOException
    {
        String randomint;
        File putfile = new File(args[0]);
        FileWriter fileWriter =  new FileWriter(putfile);
        int count =  Integer.parseInt(args[1]);
        for (int i=0; i < count; i++)
        {
            randomint = generateString();
            System.out.print(randomint + '\n');

            fileWriter.write(generateRandomString(46) + '\n');


        }
        fileWriter.close();
    }

    public static void readFromFile() throws IOException {
        FileReader fr = new FileReader("azaza.txt");
        BufferedReader br = new BufferedReader(fr);
        for (int i = 0; i<2; i++)
        {
            if (i == 1)
            {
                login = br.readLine();
            }
            else
            {
                pass = br.readLine();
            }
        }
        System.out.println(pass);
        System.out.println(login);
        fr.close();
    }



    private static  String generateRandomString(int maxlen)
    {
    int len = rnd.nextInt(maxlen);
    String s = "";
        for (int i =0; i < len; i ++)
        {
            char ch = (char) (rnd.nextInt(26) + 97);
             s= s + ch;
        }
        return s;
    }

    private boolean isElementPresent(By by) {
        try {
            driver.findElement(by);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    private boolean isAlertPresent() {
        try {
            driver.switchTo().alert();
            return true;
        } catch (NoAlertPresentException e) {
            return false;
        }
    }

    private String closeAlertAndGetItsText() {
        try {
            Alert alert = driver.switchTo().alert();
            String alertText = alert.getText();
            if (acceptNextAlert) {
                alert.accept();
            } else {
                alert.dismiss();
            }
            return alertText;
        } finally {
            acceptNextAlert = true;
        }
    }
}
